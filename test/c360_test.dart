import 'dart:math' as math;

import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Demo test for C360 app", () {
    int result = math.max<int>(2, 5);

    expect(result, 5);
  });
}
